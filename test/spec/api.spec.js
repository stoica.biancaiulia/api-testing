const { spec } = require('pactum');

describe("Comments", () => {
    const call = spec();

    it('should make a request to the json-placeholder comments page', async() => {
        await call.get('https://jsonplaceholder.typicode.com/posts/1/comments');
    });

    it('should get the first comment', async() => {
        call.withPathParams('id', '1');
    });

    it('should get the email from the first comment', async() => {
        call.withPathParams('email', 'Eliseo@gardner.biz');
    });

    it('should get a comment with a provided name', async() => {
        call.withQueryParams('name', 'alias odio sit');
    });

    it('should have header', async() => {
        call.response().to.have.header('content-type', 'application/json; charset=utf-8');
    });
    it('should have specified body', async() => {
        call.response().to.have.bodyContains('email');
    });

    it('should receive a response', async() => {
        await call.toss();
    });

    it('should have a status code of 200', async() => {
        call.response().to.have.status(200);
    });

});